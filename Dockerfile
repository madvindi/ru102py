FROM python:3.8.2-buster

RUN apt-get update

WORKDIR /src
COPY . .

RUN make env

ENV IS_CI=true
